//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

// C/C++ Libraries
#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"

// Kernel
#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"
// ----------------------------------------------------------------------------
//
// Standalone STM32F4 led blink sample (trace via DEBUG).
//
// In debug configurations, demonstrate how to print a greeting message
// on the trace device. In release configurations the message is
// simply discarded.
//
// Then demonstrates how to blink a led with 1Hz, using a
// continuous loop and SysTick delays.
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the DEBUG output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

void vTask(void *pv)
{
	uint32_t time = 0;
	trace_printf("I'm in the task");

	/* Calculate fp */
	float f = 32.0 * 3.95;
	trace_printf("\n%f", f);
	for ( ; ; )
	{
		vTaskDelay(1000);
		trace_printf("\n%d", ++time);
	}
}

int
main(int argc, char* argv[])
{
	// At this stage the system clock should have already been configured
	// at high speed.
	trace_printf("System clock: %uHz\n", SystemCoreClock);

	// Create a task
	TaskHandle_t xHandle = NULL;
	xTaskCreate(&vTask, "Task", 2048, NULL, 2, &xHandle);

	/* Start the scheduler. */
	vTaskStartScheduler();

	for ( ; ; );
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
